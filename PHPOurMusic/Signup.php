<?php
	
	require 'Database.php';

	class Signup
	{
		function _construct()
		{
		}

		public static function InsertNewDate($id,$firstname,$lastname,$gender)
		{
			$consult = "INSERT INTO Registry(id,First_Name,Last_Name,Gender) VALUES(?,?,?,?)";
			try
			{
				$result = Database::getInstance()->getDb()->prepare($consult);
				return $result->execute(array($id,$firstname,$lastname,$gender));
			}
			catch(PDOException $e)
			{
				return false;
			}
		}

		public static function InsertOnLogin($id,$password)
		{
			$consult = "INSERT INTO Login(id,Password) VALUES(?,?)";
			try
			{
				$result = Database::getInstance()->getDb()->prepare($consult);
				return $result->execute(array($id,$password));
			}
			catch(PDOException $e)
			{
				return false;
			}
		}

	}

?>
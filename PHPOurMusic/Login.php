<?php
	
	require 'Database.php';

	class Login
	{
		function _construct()
		{
		}

		public static function GetDataById($id){
			$consult = "SELECT id,Password FROM Login WHERE id = ?";

			$result = Database::getInstance()->getDb()->prepare($consult);

			$result->execute(array($id));

			$table = $result->fetch(PDO::FETCH_ASSOC);

			return $table;

		}
	}

?>
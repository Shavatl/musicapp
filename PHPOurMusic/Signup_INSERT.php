<?php
	require 'Signup.php';

	if($_SERVER['REQUEST_METHOD']=='POST')
	{
		$data = json_decode(file_get_contents("php://input"),true);
		$answer = Signup::InsertNewDate($data["id"],$data["first_name"],$data["last_name"],$data["gender"]);
		$a2 = Signup::InsertOnLogin($data["id"],$data["password"]);
		if($answer && $a2)
		{
			echo json_encode(array('result' => 'The user registered correctly'));
		}
		else
		{
			echo json_encode(array('result' => 'The user already exists, try another'));
		}
	}

?>
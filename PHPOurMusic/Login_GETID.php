<?php
	require 'Login.php';

	if($_SERVER['REQUEST_METHOD']=='GET'){

		if(isset($_GET['id'])){
			$identifier = $_GET['id'];

			$answer = Login::GetDataById($identifier);

			$contained = array();

			if($answer){
				$contained["result"] = "existing";
				$contained["data"] = $answer;
				echo json_encode($contained);
			}else{
				echo json_encode(array('result' => 'Username does not exist'));
			}
		}else{
			echo json_encode(array('result' => 'The identifier is missing'));
		}
	}
?>
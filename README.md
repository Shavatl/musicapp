# Musicapp

Esta app móvil es un reproductor de música el cual reproduce música personal desde un servidor, se creo con fines de aprendizaje al igual como proyecto escolar.

# Authors

* Salvador Quintero - (Developer, Designer) [[Gitlab](https://gitlab.com/Shavatl)]

## Others
```
Si tienes dudas o quieres contribuir con el proyecto o mejorarlo te puedes poner en contacto conmigo desde mi correo 
crispinquintero@gmail.co o por este medio, gracias.

```


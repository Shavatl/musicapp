package com.example.crisp.ourmusic.Medium;

/**
 * Created by crisp on 14/06/2018.
 */

public class Attributes
{
    private String Music_img;
    private String Name_gender;

    public Attributes()
    {}

    public String getMusic_img() {
        return Music_img;
    }

    public void setMusic_img(String music_img) {
        Music_img = music_img;
    }

    public String getName_gender() {
        return Name_gender;
    }

    public void setName_gender(String name_gender) {
        Name_gender = name_gender;
    }
}

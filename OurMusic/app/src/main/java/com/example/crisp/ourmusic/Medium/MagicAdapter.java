package com.example.crisp.ourmusic.Medium;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.crisp.ourmusic.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by crisp on 14/06/2018.
 */

public class MagicAdapter extends RecyclerView.Adapter<MagicAdapter.HolderMagic>
{

    private List<Attributes> attributesList;
    Context context;
    public MagicAdapter(List<Attributes> attributesList, Context context)
    {
        this.attributesList = attributesList;
        this.context = context;
    }

    @Override
    public HolderMagic onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cam_magic,parent,false);
        return new MagicAdapter.HolderMagic(v);
    }

    @Override
    public void onBindViewHolder(HolderMagic holder, final int position)
    {
        Picasso.with(context).load(attributesList.get(position).getMusic_img()).error(R.drawable.image).into(holder.imagen);
        holder.name.setText(attributesList.get(position).getName_gender());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return attributesList.size();
    }


    public class HolderMagic extends RecyclerView.ViewHolder
    {
        CardView cardView;
        ImageView imagen;
        TextView name;

        public HolderMagic(View itemView)
        {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardview_id);
            imagen = (ImageView) itemView.findViewById(R.id.music_img_id);
            name= (TextView) itemView.findViewById(R.id.music_title_id);
        }
    }
}

package com.example.crisp.ourmusic;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by crisp on 14/06/2018.
 */

public class StaticBase {
    public static final String shared = "ourmusic.com";
    public static final String shared_estate = "ourmusic.estado";
    public static final String shared_user = "ourmusic.usuario";


    public static void savePreferenceBoolean(Context c, boolean b, String key) {
        SharedPreferences preferences = c.getSharedPreferences(shared, c.MODE_PRIVATE);
        preferences.edit().putBoolean(key, b).apply();
    }

    public static void savePreferenceString(Context c, String b, String key) {
        SharedPreferences preferences = c.getSharedPreferences(shared, c.MODE_PRIVATE);
        preferences.edit().putString(key, b).apply();
    }

    public static boolean getPreferenceBoolean(Context c, String key) {
        SharedPreferences preferences = c.getSharedPreferences(shared, c.MODE_PRIVATE);
        return preferences.getBoolean(key, false);
    }

    public static String getPreferenceString(Context c, String key) {
        SharedPreferences preferences = c.getSharedPreferences(shared, c.MODE_PRIVATE);
        return preferences.getString(key, "");
    }
}

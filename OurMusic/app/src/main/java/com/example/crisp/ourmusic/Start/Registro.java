package com.example.crisp.ourmusic.Start;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.crisp.ourmusic.R;
import com.example.crisp.ourmusic.VolleyRP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Registro extends AppCompatActivity {

    private EditText user;
    private EditText password;
    private EditText password2;
    private EditText first_name;
    private EditText last_name;
    private RadioButton male;
    private RadioButton female;
    private Button signup;

    private VolleyRP volley;
    private RequestQueue mRequest;

    private static final String IP_REGISTRAR = "https://crispinquintero08.000webhostapp.com/PHPOurMusic/Signup_INSERT.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();

        user = (EditText) findViewById(R.id.user);
        password = (EditText) findViewById(R.id.passwor);
        password2 = (EditText) findViewById(R.id.passwor2);
        first_name = (EditText) findViewById(R.id.firstname);
        last_name = (EditText) findViewById(R.id.lastname);

        male = (RadioButton) findViewById(R.id.rdMale);
        female = (RadioButton) findViewById(R.id.rdFamale);
        male.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                female.setChecked(false);
            }
        });
        female.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                male.setChecked(false);
            }
        });

        signup = (Button) findViewById(R.id.signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String gender = "";

                if (male.isChecked()) gender = "Male";
                else if (female.isChecked()) gender = "Female";

                if(!getStringET(user).isEmpty() && !getStringET(password).isEmpty() && !getStringET(password2).isEmpty()&&
                        !getStringET(first_name).isEmpty() && !getStringET(last_name).isEmpty())
                {
                    if(getStringET(password).equals(getStringET(password2)))
                    {
                            registrarWebService(
                                    getStringET(user).trim(),
                                    getStringET(password).trim(),
                                    getStringET(first_name).trim(),
                                    getStringET(last_name).trim(),
                                    gender);
                    }
                    else
                    {
                        Toast.makeText(Registro.this,"The password is not correct.",Toast.LENGTH_SHORT).show();
                        password.setText("");
                        password2.setText("");
                    }

                }
                else
                {
                    Toast.makeText(Registro.this,"It has blank fields.",Toast.LENGTH_SHORT).show();

                }

            }
        });

    }

    private void registrarWebService(String user,String password,String firstname,String lastname,String gender){
        HashMap<String,String> hashMapToken = new HashMap<>();
        hashMapToken.put("id",user);
        hashMapToken.put("password",password);
        hashMapToken.put("first_name",firstname);
        hashMapToken.put("last_name",lastname);
        hashMapToken.put("gender",gender);

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST,IP_REGISTRAR,new JSONObject(hashMapToken), new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject datos) {
                try {
                    String estado = datos.getString("result");
                    if(estado.equalsIgnoreCase("The user registered correctly"))
                    {
                        Toast.makeText(Registro.this,estado, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(Registro.this, Login.class);
                        startActivity(i);
                        finish();

                    }
                    else
                    {
                        Toast.makeText(Registro.this,estado, Toast.LENGTH_SHORT).show();
                    }
                }
                catch (JSONException e)
                {
                    Toast.makeText(Registro.this,"Could not register.",Toast.LENGTH_SHORT).show();
                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Registro.this,"Could not register",Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,this,volley);
    }

    private String getStringET(EditText e)
    {
        return e.getText().toString();
    }
}

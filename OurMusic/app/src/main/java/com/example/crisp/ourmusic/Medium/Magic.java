package com.example.crisp.ourmusic.Medium;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.crisp.ourmusic.R;
import com.example.crisp.ourmusic.VolleyRP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Magic extends AppCompatActivity {

    private RecyclerView rv;
    private List<Attributes> attributesList;
    private MagicAdapter adapter;
    private LinearLayout empty;
    private FloatingActionButton up;

    private VolleyRP volley;
    private RequestQueue mRequest;

    private static final String URL_Gender ="https://crispinquintero08.000webhostapp.com/PHPOurMusic/Gender_GETALL.php";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magic);
        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();

        attributesList = new ArrayList<>();
        rv = (RecyclerView) findViewById(R.id.recyclerview_id);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        rv.setLayoutManager(lm);
        adapter = new MagicAdapter(attributesList,this);
        rv.setLayoutManager(new GridLayoutManager(this,2));
        rv.setAdapter(adapter);

        up= (FloatingActionButton) findViewById(R.id.update);
        up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toupdate();
            }
        });

        requestJSON();
        empty = (LinearLayout) findViewById(R.id.emptyl);

    }
    public void verifystatus()
    {
        if(attributesList.isEmpty())
        {
            empty.setVisibility(View.VISIBLE);
            rv.setVisibility(View.GONE);
        }
        else
        {
            empty.setVisibility(View.GONE);
            rv.setVisibility(View.VISIBLE);
        }
    }

    public void addGender(String img,String name)
    {
        Attributes attribut = new Attributes();
        attribut.setMusic_img(img);
        attribut.setName_gender(name);
        attributesList.add(attribut);
        toupdate();
    }

    public void toupdate()
    {
        adapter.notifyDataSetChanged();
        verifystatus();

    }

    public void requestJSON()
    {
        JsonObjectRequest requets = new JsonObjectRequest(URL_Gender, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject dato)
            {
                try
                {
                    String AllDate = dato.getString("result");
                    JSONArray jsonArray = new JSONArray(AllDate);
                    for(int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject js = jsonArray.getJSONObject(i);
                        addGender("https://crispinquintero08.000webhostapp.com/Music/Generos/"+js.getString("Img"),
                                js.getString("Gender"));

                    }
                }
                catch (JSONException e)
                {}

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
            }
        });
        VolleyRP.addToQueue(requets,mRequest,this,volley);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        toupdate();
    }
}

package com.example.crisp.ourmusic.Start;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.crisp.ourmusic.Medium.Magic;
import com.example.crisp.ourmusic.R;
import com.example.crisp.ourmusic.StaticBase;
import com.example.crisp.ourmusic.VolleyRP;

import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {

    private EditText usuario;
    private EditText pasword;
    private RadioButton sesion;
    private Button login;
    private TextView returnsignup;

    private String User="";
    private String Password="";
    private boolean activated;

    private VolleyRP volley;
    private RequestQueue mRequest;

    private static String IP = "https://crispinquintero08.000webhostapp.com/PHPOurMusic/Login_GETID.php?id=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();

        usuario = (EditText) findViewById(R.id.Etuser);
        pasword = (EditText) findViewById(R.id.Etpassword);
        sesion = (RadioButton) findViewById(R.id.rbSesion);
        activated = sesion.isChecked();
        sesion.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(activated)
                {
                    sesion.setChecked(false);
                }
                activated = sesion.isChecked();

            }
        });

        returnsignup = (TextView) findViewById(R.id.returnsingup);
        returnsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, Registro.class);
                startActivity(i);
            }
        });

        login = (Button) findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check(usuario.getText().toString(),pasword.getText().toString());

            }
        });
    }

    public void check(String user, String password)
    {
        User = user;
        Password = password;
        requestJSON(IP+user);
    }

    public void requestJSON(String URL)
    {
        JsonObjectRequest requets = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject data)
            {
                checkLoging(data);

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(Login.this,"An error occurred.",Toast.LENGTH_SHORT).show();

            }
        });
        VolleyRP.addToQueue(requets,mRequest,this,volley);
    }

    public void checkLoging(JSONObject data)
    {
        try
        {
            String estado = data.getString("result");
            if(estado.equals("existing"))
            {
                JSONObject Jsondatos = new JSONObject(data.getString("data"));
                String user = Jsondatos.getString("id");
                String pass = Jsondatos.getString("Password");

                if(user.equals(User) && pass.equals(Password))
                {
                    StaticBase.savePreferenceBoolean(Login.this,sesion.isChecked(),StaticBase.shared_estate);
                    StaticBase.savePreferenceString(Login.this,User,StaticBase.shared_user);
                    login();
                }
                else
                {
                    Toast.makeText(this,"Incorrect password",Toast.LENGTH_SHORT).show();

                }

            }
            else
            {
                Toast.makeText(this,"There is no user",Toast.LENGTH_SHORT).show();
            }
        }
        catch (JSONException e) {}
    }

    public void login()
    {

        final ProgressDialog progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        Intent i = new Intent(Login.this, Magic.class);
                        startActivity(i);
                        finish();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }

}
